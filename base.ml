let () =
  let argv' = Glut.init Sys.argv in
  ignore (Glut.createWindow ~title:"OpenGL Demo");
  GlClear.color (0.1, 0.3, 0.1);
  Glut.displayFunc ~cb:(fun () -> GlClear.clear [ `color ]; Gl.flush ());
  Glut.mainLoop ()
