(*OCaml version 4.01.0*)

List.length ["one";"two";"three"];;

let f i = match i with
    0 -> "Zero"
    3 -> "Three"
    -> "Neither zero nor three";;
f 0;;
let a_tuple = (3,"three");;
let another_tuple = (3,"four",5.);;
let (x,y) = a_tuple;;
x + String.length y;;
let distance (x1,y1) (x2,y2) =
    sqrt((x1 -. x2) ** 2. +. (y1 -. y2) ** 2.);;
let languages = ["Ocaml";"Perl";"C"];;
List.length languages;;
let numbers = [3;"four";5];;
List.map languages ~f:String.length;;
List.map ~f:String.length languages;;

"French"::"Spanish"::languages;;

let rec sum l =
  match l with
    | [] -> 0
    | hd :: tl -> hd + sum tl;;

sum [1;2;3];;

let rec destutter list = 
  match list with
  | [] -> []
  | [hd] -> [hd]
  | hd1 :: hd2 :: tl ->
     if hd1 = hd2 then destutter (hd2::tl)
     else hd1 :: destutter (hd2::tl);;
destutter ["hey";"hey";"hey";"hey";"man!"];;
